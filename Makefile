install:
	@echo "Installing ldnscript"
	install -o root -g wheel -d /var/ldnscript
	install -o root -g wheel -d /etc/ns
	install -o root -g bin -m 0755 ldnscript /usr/local/sbin/
	install -o root -g wheel -m 0600 -C ldnscript.conf /etc/ns/ldnscript.conf

uninstall:
	@echo "Removing ldnscript"
	rm /usr/local/sbin/ldnscript
