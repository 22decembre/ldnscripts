#!/bin/ksh

# Version 3

# Copyright (c) 2017 Stéphane Guedon "22Decembre" <stephane @ 22decembre.eu>

# Permission to use, copy, modify, and distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

# Alg 1, 3 and 6 shall not be used

set -A ALG_NUM "005" "007" "008" "010" "012" "013" "014" "015"
set -A ALG_NAMES "RSASHA1" "RSASHA1-NSEC3-SHA1" "RSASHA256" "RSASHA512" "ECC-GOST" "ECDSAP256SHA256" "ECDSAP384SHA384" "ED25519"

ROOT="/var/ldnscript"
NS_REP="/etc/ns"

[ -r "$NS_REP/ldnscript.conf" ] && . "$NS_REP/ldnscript.conf"

# check for good number of args
if [ $# -ne 2 ]; then
	_help()
fi

ACTION="$(echo $1| tr [:upper:] [:lower:])"
DOMAIN="$(echo $2| tr [:upper:] [:lower:])"

[ -r $NS_REP/$DOMAIN.conf ] && . $NS_REP/$DOMAIN.conf

DIR_ZONE="$ROOT/$DOMAIN"

launcher() {
    case "$ACTION" in
		"ksk"|"zsk")		
            DATO=$(date +%d)
            if [ $DATO -lt 15 ]; then
                TYPE="$ACTION"
                _keygen generated
            else
            	echo "Wait for next month (change the algo in the conf if you need)."
            fi ;;

		"init"|"initialize") _init ;;
        	"rollover") _roll ;;
		"sign"|"signing") _signing ;;
		"check") _check;;
		"status") _status;;
		"help"|"") _help;;
    esac
}

_help() {
	echo "$0 action domain"
	echo "action: sign|check|init|ksk|zsk|rollover"
	echo "Please, type 'ldnscript help' followed by the action you need."
	echo "-------------------------------------------------------------"
	
	# DOMAIN is second word argument so the script will echo its help right away.
	case "$DOMAIN" in
		"check")
			echo "usage : $0 check domain"
			echo "Will check the current status of your DNSSEC signed zone according to a DNS resolver picked out randomly from a list."
		;;
		"init")
			echo "Please, provide the zone name as: $0 init example.com"
			echo "Will create the necessary directories and keys for the given domain, then trigger a signing and loading into NSD."
		;;
		"ksk")
			echo "syntaxe : $0 ksk|zsk zone"
			echo "This will create a key of said type for the zone. You need to order a signing yourself just after."
			echo "example : $0 zsk example.org"
		;;
		"rollover")
			echo "usage: $0 rollover domain"
			echo "You shall run this command at a regular interval."
			echo "This will rotate your keys. It will erase all old keys. It will then move current active zsk key to old state and move waiting key to active state and then generate a new zsk."
			echo "If a new ksk is present, the current one will be moved to old state and the new put to active."
			echo "This command will finally trigger a signing operation and therfore a reloading of NSD on said domain."
		;;
		"signing")
            echo "usage: $0 signing domain"
            echo "This will load the current zonefile with all the keys, copy it with a new serial number, sign and reload NSD."
            echo "You should run this command a each modification of your domain."
                ;;
		"status")
			echo "usage : $0 status domain"
		;;
	esac
	exit 0
}

_status_type() {
cd "$DIR_ZONE/$TYPE"

echo "### $TYPE"
for status in generated private retire; do
echo "## $status"
stat -f "creation: %t%Sm %N" *.$status
done
}

_serial() {

PREVIOUS=$(head -n1 "/var/nsd/zones/signed/$DOMAIN" |awk '{print $7}')

case "$SERIAL" in
		"date")
		NOW=$(date +%Y%m%d)
		PREVIOUS_DATE=$(echo "$PREVIOUS" | cut -c1-8)
		
		if [ "$PREVIOUS_DATE" = "$NOW" ]; then
            		SERIAL=$(($PREVIOUS+1))
		else
            		SERIAL="$NOW"01
		fi
		;;
		
		"incremental") 
		(($SERIAL=$PREVIOUS+1))
		;;
    esac
}

_status() {
ZONEFILE="/var/nsd/zones/signed/$DOMAIN"

echo "$DOMAIN"

TYPE="zsk"
_status_type

echo " "

TYPE="ksk"
_status_type

echo " "
stat -f "Last signature: %t%Sm %N" $ZONEFILE

_verify
}

_check() {

# opendns: semble pas faire de valid' resolver1.opendns.com
# fdn : semble valider seulement sur le ns1

set -A RESOLV recpubns2.nstld.net ns1.sprintlink.net \
	google-public-dns-a.google.com \
	ns1.fdn.fr recursif.arn-fai.net \
	anycast.censurfridns.dk \
	ns0.ldn-fai.net \
	resolver2.dns.watch \
	bind.odvr.dns-oarc.net \
	unbound.odvr.dns-oarc.net \
	dns.quad9.net dnsres1.nic.cz \
	1dot1dot1dot1.cloudflare-dns.com \
	ns1.recursive.dnsbycomodo.com ordns.he.net \
	ns.servidordenoms.cat rdns.dynect.net

COUNT=${#RESOLV[@]}

# generate random value segun count
r="$(echo "$RANDOM%$COUNT" | bc)"
SERVER="${RESOLV[$r]}"

unset COUNT r

answer=$(/usr/sbin/dig +dnssec +short -t soa @"${SERVER}" "$DOMAIN")
if [ "$answer" ]; then
    if [ "$(echo "$answer" | grep "SOA" )" ]; then
        echo "According to ${SERVER}, $DOMAIN is secure. Everything is ok."
    else
        echo "According to ${SERVER}, $DOMAIN is not DNSSEC valid, but still resolvable via DNS."
    fi
else
    echo "According to ${SERVER}, $DOMAIN is bogus."
fi
}

#  Usage: _search_key type state
#  state = private OR generated OR retire
# Supposed to work in a case where you already work with either TYPE=zsk or ksk.
#  Result: array LIST with the first one being the last generated 
_search_key() {
    STATE="$1"

    unset LIST
    cd "$DIR_ZONE/$TYPE"
    
    i=0
    for x in *.$STATE ;  do
        LIST[$i]=$(basename $x .$STATE)
        ((i++))
    done
    
    unset STATE
}

#  Usage: _preselect state
#  state = private OR generated OR retire
# Supposed to work in a case where you already work with either TYPE=zsk or ksk.
# select the key corresponding best to your definition : the last with the good algorithm, or the last one.
_preselect() {  
    STATE="$1"
    
    ## If the Algo is provided as a number, then we can use it directly
    if [ $ALG -ge 0 ]; then
         NUM="$ALG"
    else
    
    i=0
    while [ ${ALG_NAMES[$i]} != $ALG ]
        do ((i++))
    done
    NUM=${ALG_NUM[$i]}
    unset i
    
    fi

    NAME="K$DOMAIN.+$NUM+"
    
    cd $DIR_ZONE/$TYPE
        
        KEY=$(find "$DIR_ZONE/$TYPE" -type f -name "$NAME*.$STATE" -prune | head -n1)
        if [ -z "$KEY" ]; then
            KEY=$(find "$DIR_ZONE/$TYPE" -type f -name "*.$STATE" -prune | head -n1)
        fi
        
    if [ "$KEY" ]; then
    KEY=$(basename $KEY .$STATE)
    fi
    
    unset STATE
}

# just use previous fonction and check whether or not we have a key of said type/state
_select() {
STATE="$1"

_preselect $STATE
if [ -z "$KEY" ]; then
     echo "You don't have a $STATE $TYPE for $DOMAIN. Check about it !"
fi
}

#  Usage: _dead state
# Supposed to work in a case where you already work with either TYPE=zsk or ksk.
# erase all keys of said type
_dead() {
STATE="$1"

if [ "$(ls $DIR_ZONE/$TYPE/*.$STATE)" ]; then
    set -- *.$STATE
    for key in *.$STATE; do
        z=$(basename $key .$STATE)
        [ "$VERBOSE" -eq 1 ] && echo "Removing $key"
        rm -f "$DIR_ZONE/$TYPE/$z".{key,ds,$STATE}
    done
fi
unset STATE
}

_roll() {

mkdir -p "$DIR_ZONE"/{z,k}sk

TYPE="zsk"
cd $DIR_ZONE/$TYPE

_dead retire

# mv private to retire
#_search_key private
for x_zsk in *.private; do
    mv -- "$x_zsk" "${x_zsk%.private}.retire"
    [ "$VERBOSE" -eq 1 ] && echo "setting $x_zsk to retire"
done

# select next key and mv to private
_select generated
[ "$VERBOSE" -eq 1 ] && echo "setting $KEY to active state"
mv $DIR_ZONE/$TYPE/$KEY.generated $DIR_ZONE/$TYPE/$KEY.private

_dead generated

# new
_keygen generated

###########

TYPE="ksk"
cd $DIR_ZONE/$TYPE

_dead retire

# let's check : do we have new generated keys ? if yes : mv them to work
_preselect generated
if [ "$KEY" ]; then
    echo $KEY
    
    for x_ksk in *.private; do
        mv -- "$x_ksk" "${x_ksk%.private}.retire"
        [ "$VERBOSE" -eq 1 ] && echo "setting $x_ksk to retire"
    done

    #_select generated
    [ "$VERBOSE" -eq 1 ] && echo "setting $KEY to active state"
    mv $DIR_ZONE/$TYPE/$KEY.generated $DIR_ZONE/$TYPE/$KEY.private
fi

_signing
}

#  Usage: TYPE="type" _keygen state (state is optional)
#  Result: it create the key of said type. If state is given, the private key is moved to it.
_keygen() {
    mkdir -p "$DIR_ZONE"/{z,k}sk

    STATE="$1"
    
    case $TYPE in
    zsk )
	ARG="-a $ALG -b $ZSK_BITS $DOMAIN";;

    ksk )
	ARG="-k -a $ALG -b $KSK_BITS $DOMAIN";;
    esac
    
    cd "$DIR_ZONE/$TYPE"

	KEY="$(/usr/local/bin/ldns-keygen $ARG)"
	
	if [ -n "$KEY" ]; then
		chmod 400 $KEY.private
		[ $VERBOSE -eq 1 ] && echo "The key $KEY has been generated with these arguments : $ARG"	
	else 
		echo "Error at key generation"; exit 1
	fi
	
	if [ -n "$STATE" ]; then
        mv ${KEY}.private ${KEY}.$STATE
    fi
	
    case "$TYPE" in
	"ksk")
		echo "A new KSK key has been generated."
		echo "Make sure to update the DS record in your registrar quickly."
		echo "The key is $KEY"
		echo "DS content : "; cat "${KEY}.ds"

		cat "$DIR_ZONE"/ksk/*.ds > "$DIR_ZONE/ds"
		chmod +r "$DIR_ZONE/ds"
	;;
	esac
	
	unset STATE
}

#  Usage: _init - call only by init menu!
#  Result: to initialize key and signing domain
_init() {

echo "This script will initialize your zone $DOMAIN with the general configuration or the one set at $NS_REP/$DOMAIN.conf."
echo "If you are not happy with it, modify your configuration (by copying the conf file to $NS_REP/$DOMAIN.conf and then editing it) and run this script again."
echo "The script will create one KSK and two ZSK and finally sign the zone (which will triger a reload of your NSD server on the $DOMAIN zone)."

mkdir -p /var/nsd/zones/signed/
mkdir -p "$DIR_ZONE"/{z,k}sk

TYPE="zsk"
_keygen generated
_keygen

TYPE="ksk"
_keygen

unset TYPE

_signing
}

_verify() {
/usr/local/bin/ldns-verify-zone -S -k "$DIR_ZONE/ds" "$ZONEFILE"
}

_signing() {

_serial

[ $VERBOSE -eq 1 ] && echo "create a complete zone file for $DOMAIN with the current time as SOA"
/usr/local/bin/ldns-read-zone -S "$SERIAL" "$NS_REP/$DOMAIN" > "$DIR_ZONE/tmp"

# ... with all the keys in the zone
cat "$DIR_ZONE"/ksk/*.key >> "$DIR_ZONE/tmp"
cat "$DIR_ZONE"/zsk/*.key >> "$DIR_ZONE/tmp"

DEBUT=$(date +%s)
FIN=$(($DEBUT+$VALIDITY * 24 * 3600))

# we sign with the first key that is active with the good algorithm.
# If several keys -> owners get to tidy the place
# WARNING : no check of age of keys !

TYPE="zsk" _select private
ZSK=$KEY

TYPE="ksk" _select private
KSK=$KEY

ZONEFILE="$DIR_ZONE/signed"

[ $VERBOSE -eq 1 ] && echo "Signing zone"
/usr/local/bin/ldns-signzone -i $DEBUT -e $FIN \
	-n -s $(head -n 1000 /dev/random | sha256 | cut -b 1-16) \
	-t $RUN -o $DOMAIN -f $ZONEFILE $DIR_ZONE/tmp $DIR_ZONE/ksk/$KSK $DIR_ZONE/zsk/$ZSK

if _verify; then
        [ $VERBOSE -eq 1 ] && echo "Move new file in place and reloading nsd"
        cp $ZONEFILE "/var/nsd/zones/signed/$DOMAIN"
		/usr/sbin/nsd-control reload "$DOMAIN"
		/usr/sbin/nsd-control notify "$DOMAIN"
else
        [ $VERBOSE -eq 1 ] && echo "Error in zone, current zone file not replaced not reloading nsd"
fi
}

#launcher

if [ "$DOMAIN" = "all" ]; then
DOMAIN=""
    for DOM in `ls $ROOT`; do
         $0 $ACTION $DOM
    done
    
else
    launcher
fi
