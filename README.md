# README

(English version after the French)

Ldnscript est un script Korn shell écrits pour permettre la maintenance d'une zone DNSSEC simple sous OpenBSD.

Ce README constitue une description succinte de son installation et utilisation. Pour une description plus détaillée, l'auteur a écrit des articles sur son blog :

https://www.22decembre.eu/fr/2017/11/01/ldnscripts/

## installation

Il vous faut télécharger l'archive ou le [git](https://framagit.org/22decembre/ldnscripts), puis lancer **make install** en tant que root.

    $ cd /usr/local/src/
    $ doas mkdir ldnscripts
    $ doas chown $USER ldnscripts
    $ git clone https://framagit.org/22decembre/ldnscripts.git
    $ cd ldnscripts
    $ doas make install

Il vous faut également le paquet ldns-utils, présent dans les dépots OpenBSD :

    $ doas pkg_add ldns-utils

## configuration

Le fichier de configuration s'appelle *ldnscript.conf*, dans le dossier */etc/ns/*. Il contient notamment l'emplacement des fichiers de zones sources (au format DNS standard. Si vous utilisez un $ORIGIN, mettez le en tête du fichier de zone.), les longueurs de clés et l'algorithme à utiliser.

Le fichier de configuration contient aussi le numéro de série à utiliser:
 - **date**, ce qui donnera AAAAMMJJNN où NN est un numéro incrémental. Exemple: 2018122003.
 - **incremental**, ce qui donnera juste un nombre indiquant le nombre de fois où la zone a été signée.
 - **unixtime** (choix par défaut), qui utilisera le *timestamp* unix du moment de la signature.

Les algorithmes disponibles sont ceux de ldns :

    $ ldns-keygen -a list

La durée de validité des signatures, indiquée en jours via VALIDITY doit imperativement être plus importante que votre intervalle de signature.

## Utilisation

Le script requiert deux arguments : l'action, et le nom de la zone.

    $ doas ldnscript sign example.com

Les actions sont décrites ci après. Il s'agit de sign, ksk, zsk, init, rollover, check et status. Si vous utilisez le mot-clé **all** en lieu et place du nom de la zone, le script executera l'action demandée pour toutes les zones.

## NSD

Les fichiers de zones signés finaux seront écrits dans /var/nsd/zones/signed/$ZONE. Lors de la signature, le script appellera un rechargement de la zone par NSD.
Vous devez configurez NSD de cette façon :

    ## master zones
    zone:
        name: "zone.tld"
        zonefile: "signed/zone.tld"

## roulement de clés

La commande *rollover* doit être activée chaque mois (pour mettre en activité et créer de nouvelles clés ZSK). Les clés n'ont pas (encore) de durée de validité. Il faut que vous copiez-collez ceci dans votre */etc/monthly.local* :

    /usr/local/sbin/ldnscript rollover all

Ceci tient compte du cas où vous avez plusieurs domaines sur votre serveur.

## Creer de nouvelles KSK|ZSK

La creation de nouvelles KSK (ou ZSK) doit se faire manuellement avec la commande ksk, avant le 15 du mois courant.

    # ldnscript ksk domain.tld
    # ldnscript signing domain.tld

Pensez à enregistrer le DS dans le registre tout de suite. La nouvelle KSK sera mise en activité dès le prochain roulement de clés.

## signatures

La commande *sign* ou *signing* doit être lancée à chaque changement dans la zone et après avoir crée une nouvelle clé. Elle doit également être lancée à intervalle régulier via une commande cron (si vous avez un ou plusieurs domaines à signer en même temps) :

    # Cron
    # signature dnssec tous les 5 jours à 00:08
    8       0 */5 *       *       /usr/local/sbin/ldnscript sign all

Ou (signature d'un seul domaine, un jour sur deux):

    8       0 */2       *       *       /usr/local/sbin/ldnscript signing domain.tld

La fréquence de signature est un choix personnel. Faîtes le tous les jours si vous le voulez. Mais mettez votre VALIDITY en correspondance.

Après la signature, la zone est vérifiée et si elle est valide, elle est alors copiée dans le chroot de NSD et rechargée.

## lancer une zone

Vous placez votre fichier de zone initial dans le dossier indiqué via NS_REP dans conf. Vous ajoutez un eventuel fichier de configuration specifique au domaine juste à côté, puis vous lancez init:

    # ldnscript init domain.tld
    
Le script va créer les repertoires, puis les clés et signer la zone une première fois. Plus qu'à enregistrer le DS dans le registre supérieur.

## check

    $ ldnscript check 22decembre.eu
    According to ns0.ldn-fai.net, 22decembre.eu is secure. Everything is ok.

Check va vérifier auprès d'un resolveur-validateur DNS (pris au hasard dans une liste de quatorze, parmi lesquels on a Google et Quad, mais aussi des FAI de FFDN) que votre zone est bien valide.

Vous pouvez lancer ce script aussi souvent que necessaire, également sur des zones que vous ne controllez pas, mais soyez un bon citoyen (ces resolvers pourraient vous bannir).

## status

Status va lister toutes les clés dans votre zone, vous indiquer quand a eu lieu la dernière signature et si le fichier de zone signée actuel est bien valide.

    $ ldnscript status 22decembre.eu                                                 
    ### zsk
    ## generated
    creation:       Oct  1 16:05:19 2017 K22decembre.eu.+010+45395.generated
    ## private
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+50516.private
    ## retire
    stat: *.retire: No such file or directory
    
    ### ksk
    ## generated
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+55059.generated
    ## private
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+49366.private
    ## retire
    stat: *.retire: No such file or directory
    
    Last signature:         Oct 26 00:08:01 2017 /var/nsd/signed/22decembre.eu
    Zone is verified and complete

########################

Ldnscript is a Korn shell script written to run my DNSSEC zones under OpenBSD.

This README is a rather short description of the scripts. For a full description, you can read the author's blog:

https://www.22decembre.eu/en/2017/11/01/ldnscripts/

## installation

Installation can be done just by downloading from [framagit](https://framagit.org/22decembre/ldnscripts), then running **make install** as root :

    $ cd /usr/local/src/
    $ doas mkdir ldnscripts
    $ doas chown $USER ldnscripts
    $ git clone https://framagit.org/22decembre/ldnscripts.git
    $ cd ldnscripts
    $ doas make install

You also need ldns-utils, in OpenBSD repositories:

    $ doas pkg_add ldns-utils

## configuration

The configuration file is called *ldnscript.conf*, in */etc/ns*, and contains the place for the original zonefiles (in the DNS standard format. If you use an $ORIGIN, place it atop the zonefile.), keys lengths and algorithms to use.

The configuration file also contains the type of serial number to use:
 - **date**, which will give YYYYMMDDNN where NN is an increment number. Example: 2018122003.
 - **incremental**, which will just give a number stating the number of times this zone had been signed.
 - **unixtime** (by default), which will use the unix *timestamp* of the signature.

The possible algorithms are those of ldns:

    $ ldns-keygen -a list

Validity of signatures is written in days in VALIDITY and must be higher than the frequency you sign your domain.

## Use

The script requires two arguments: action and zonename.

    $ doas ldnscript sign example.com

Actions are described below. They can be : sign, check, status, init, ksk, zsk and rollover. If you use the keyword **all** in place of domain name, the script will performe the requested action for all the zones.

## NSD

Signed zone files will be copied in /var/nsd/zones/signed/$ZONE. Just after its writing operation, the signing command will trigger an NSD reload on the zone.
It must be setup this way:

    ## master zones
    zone:
        name: "zone.tld"
        zonefile: "signed/zone.tld"
        
## keys rollover

The rollover script has to run every month (to activate and create new ZSK). The rollover will trigger a signing at the end (see below). Keys don't have validity periods (yet). You just have to copy this to your */etc/monthly.local* :

    /usr/local/sbin/ldnscript rollover all

## Create new KSK

Creation of new KSK have to be done manually with the keygen script, before the 15th of the month.

    # ldnscript ksk domain.tld
    # ldnscript signing domain.tld

Think about registering their DS in the TLD already. The new KSK will get activated at the next rollover.

## signatures

The signing has to run each time you have news in the zone and after creating a new key. It has to run with a cron task:

    # Cron
    # dnssec signature every 5 days 00:08 for all domains.
    8       0 */5 *       *       /usr/local/sbin/ldnscript sign all

Or:

    # signing a single domain every second day.
    8       0 */2       *       *       /usr/local/sbin/ldnscript signing domain.tld
    
Signing frequency is personnal choice, but it must be done accordingly to the VALIDITY option (VALIDITY must be higher).

## launch a zone

You place your raw file in the directory set in NS_REP. You add a specific conf file aside if need be, then run init:

    # ldnscript init domain.tld
    
The script will create repositories, keys and then sign it once. You just have to register the DS in the upper zone (probably a registry, and you have to use your registar procedure).

## check

    $ ldnscript check 22decembre.eu
    According to ns0.ldn-fai.net, 22decembre.eu is secure.

Check will validate your zone after a random DNS resolver in a list of fourteen (Google or Quad DNS, but also french free ISP) and give you the results. You may run this command as often as it fits you, even on zones you don't control (it does not look after any file on your own server), but I cannot guarantee that the resolvers won't ban you after...

## status

Status will list all the keys in the zone and tell you when the last signing operation occured and if the current zone file is valid.

    $ ldnscript status 22decembre.eu                                                 
    ### zsk
    ## generated
    creation:       Oct  1 16:05:19 2017 K22decembre.eu.+010+45395.generated
    ## private
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+50516.private
    ## retire
    stat: *.retire: No such file or directory
    
    ### ksk
    ## generated
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+55059.generated
    ## private
    creation:       Oct  1 16:01:56 2017 K22decembre.eu.+010+49366.private
    ## retire
    stat: *.retire: No such file or directory
    
    Last signature:         Oct 26 00:08:01 2017 /var/nsd/signed/22decembre.eu
    Zone is verified and complete
